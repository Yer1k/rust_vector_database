use qdrant_client::client::QdrantClient;
use qdrant_client::qdrant::{PointStruct, CreateCollection, SearchPoints, Distance, VectorParams, VectorsConfig, vectors_config::{Config}};
use serde_json::json;

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    // Create Qdrant client
    let client = QdrantClient::from_url("http://localhost:6334").build()?;

    // Create a collection with vector parameters if it doesn't exist
    let collection_name = "city_collection";
    if !client.collection_exists(collection_name).await? {
        let create_collection_request = CreateCollection {
            collection_name: collection_name.to_string(),
            vectors_config: Some(VectorsConfig {
                config: Some(Config::Params(VectorParams {
                    size: 4,
                    distance: Distance::Dot.into(), // Specify the distance metric here
                    ..Default::default()
                })),
            }),
            ..Default::default()
        };
        client.create_collection(&create_collection_request).await?;
    }

    // Define points to insert
    let points = vec![
        PointStruct::new(
            1,
            vec![0.13, 0.77, 0.82, 0.21], // New point vector
            json!({"city": "London"}).try_into().map_err(|e| format!("Payload conversion error: {}", e))?, // Metadata associated with the point
        ),
        PointStruct::new(
            2,
            vec![0.85, 0.89, 0.64, 0.93], // New point vector
            json!({"city": "Chengdu"}).try_into().map_err(|e| format!("Payload conversion error: {}", e))?, // Metadata associated with the point
        ),
        PointStruct::new( // Corrected variable type
            3,
            vec![0.11, 0.77, 0.82, 0.21], // New point vector
            json!({"city": "Paris"}).try_into().map_err(|e| format!("Payload conversion error: {}", e))?, // Metadata associated with the point
        ),
        PointStruct::new(
            4,
            vec![0.81, 0.32, 0.64, 0.93], // New point vector
            json!({"city": "New York"}).try_into().map_err(|e| format!("Payload conversion error: {}", e))?, // Metadata associated with the point
        ),
        PointStruct::new(
            5,
            vec![0.85, 0.89, 0.64, 0.94], // New point vector
            json!({"city": "Chongqing"}).try_into().map_err(|e| format!("Payload conversion error: {}", e))?, // Metadata associated with the point
        ),
    ];
    // Insert points into the collection
    let insertion_result = client.upsert_points_blocking(collection_name.to_string(), None, points.clone(), None).await?;
    println!("Insertion result: {:?}", insertion_result);

    // Search for points in the collection
    let search_request = SearchPoints {
        collection_name: collection_name.to_string(),
        vector: vec![0.11, 0.77, 0.82, 0.21], // Search vector
        limit: 10, // Set a valid limit value here
        ..Default::default()
    };
    let search_result = client.search_points(&search_request).await?; // Corrected variable type
    println!("Search result: {:?}", search_result);

    Ok(())
    }