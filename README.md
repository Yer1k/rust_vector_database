# Rust Vector Database using Qdrant

[![CI/CD status](https://gitlab.com/Yer1k/rust_vector_database/badges/main/pipeline.svg)](https://gitlab.com/Yer1k/rust_vector_database/-/commits/main)

## Demo
**Click on the image** to see the demo of the application on YouTube. The demo shows how to run the application, add vectors to the database, search for the nearest neighbors of a given vector, and visualize the vectors in the database
[![demo](screenshots/demo.png)](https://youtu.be/r8oTzUaxBWs)

## Description
This is a simple vector database application written in Rust using the Qdrant library. The database stores vectors in a high-dimensional space and allows you to search for the nearest neighbors of a given vector.

Say we want to create a databse for different cities. We can represent each city as a vector of 4 elements, e.g. city population, average temperature, average income, and crime rate. We can then search for the nearest neighbors of a given city. For example, if we search for the nearest neighbors of Chengdu, the database might return Chongqing.

![vectors](screenshots/vectors.png)
![chengdu](screenshots/chengdu.png)
![search](screenshots/search.png)

## How to run
1. Install Rust: https://www.rust-lang.org/tools/install

2. Install Docker: https://docs.docker.com/get-docker/

3. Pull Qdrant docker image:
```bash
docker pull qdrant/qdrant
```

4. Run Qdrant image in a container:
```bash
 docker run -p 6333:6333 -p 6334:6334 \
    -e QDRANT__SERVICE__GRPC_PORT="6334" \
    qdrant/qdrant
```
![docker](screenshots/docker.png)
![qdrant](screenshots/run_qdrant.png)

5. Run the database:
```bash
cargo run
```
![run](screenshots/run.png)

6. Open the website in your browser: http://localhost:6333/dashboard
![dashboard](screenshots/dashboard.png)

## Features
- Add vectors to the database
- Search for the nearest neighbors of a given vector
- Delete vectors from the database
- Update vectors in the database
- List all vectors in the database
- Visualize the vectors in the database

## Screenshots
Search and Add:
![search and insert](screenshots/insert_search.png)

Database info:
![info](screenshots/info.png)

Database Snapshot:
![snapshot](screenshots/snapshot.png)

Database Visualization:
![visualization](screenshots/visual.png)

